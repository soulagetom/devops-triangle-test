
public class Triangle {
	
	private int c1;
	private int c2;
	private int c3;
	
	
	public Triangle(int c1, int c2, int c3) {
		
		if (c1==0 || c2==0 || c3==0){
			return;
		}
	
		if(c1<0) {
			c1 = (-1)*c1;
		}
		
		if(c2<0) {
			c2 = (-1)*c2;
		}
		
		if(c3<0) {
			c3 = (-1)*c3;
		}	
		
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
	}
	
	public boolean IsCoherent() {
		return ((c1>0) && (c2>0) && (c3>0) && (c1+c2>c3) && (c1+c3>c2) && (c2+c3>c1));
	}
	
	public boolean IsIsocele() {
		return ((c1 == c2 && c2 != c3) || (c2 == c3 && c1 != c3) || (c1 == c3 && c3 != c2));
	}
	
	public boolean IsEquilateral() {
		return(c1==c2 && c2==c3);
	}
	
	public boolean IsRectangle() {
		return ( ( (c1*c1) + (c2*c2) == c3*c3 ) || ( (c2*c2) + (c3*c3) == (c1*c1) ) ||  ( (c1*c1) + (c3*c3) == (c2*c2) ));
	}
	
	
	
}
