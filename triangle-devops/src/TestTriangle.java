import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;

public class TestTriangle {
	private Triangle t;
	private Triangle t1;
	private Triangle t2;
	private Triangle t3;
	private Triangle t4;
	private Triangle t5;
	private Triangle t6;
	
	@BeforeAll
	public static void debut() {
		System.out.println("Test Triangle : CA COMMENCE");
	}
	
    @AfterAll
    public static void fin() {
    	System.out.println("Test Triangle : C'EST TERMINE");
    }
    
    @BeforeEach
    public  void init() {
        System.out.println("init...");	
        t = new Triangle(-2, 4, 8);
        t1 = new Triangle(3, 4, 5);
		t2 = new Triangle(5, 6, 6);
		t3 = new Triangle(5, 5, 5);
		t4 = new Triangle(-3, 4, 5);
		t5 = new Triangle(0, 0, 0);
    }
    
    @AfterEach
    public void del() {
        System.out.println("...del");
        t = null;
        t2 = null;
        t3 = null;
        t4 = null;
    }
    
	@Test
	@Tag("TestIsCoherent")
	@Order(1)
	public void testVerifTriangle() {
		Assertions.assertEquals(false, t.IsCoherent(),  "Ce n'est pas un triangle");
		Assertions.assertEquals(true, t2.IsCoherent(),  "C'est un triangle");
		Assertions.assertEquals(false, t.IsCoherent(),   "Ce n'est pas un triangle car il a des valeurs null");
	}
	
	@Test
	@Tag("TestEquilateral")
	@Order(2)
	public void testEquilateral() {
//		t2 = new Triangle(5, 6, 6);
//		t3 = new Triangle(5, 5, 5);
		Assertions.assertTrue(t3.IsEquilateral(),  "Triangle Equilateral");
		Assertions.assertFalse(t2.IsEquilateral(),  "Pas Triangle Equilateral");
	}
	
	@Test
	@Tag("TestIsocele")
	@Order(3)
	public void testIsocele() {
//		t2 = new Triangle(5, 6, 6);
//		t3 = new Triangle(5, 5, 5);
		Assertions.assertTrue(t2.IsIsocele(),  "Triangle Isoceles");
		Assertions.assertFalse(t3.IsIsocele(),  "Pas Triangle Isoceles");
	}
	
	@Test
	@Tag("TestRectangle")
	@Order(3)
	public void testRectangle() {
//		t2 = new Triangle(5, 6, 6);
//		t3 = new Triangle(5, 5, 5);
		Assertions.assertTrue(t1.IsRectangle(),  "Triangle Rectangle");
		Assertions.assertFalse(t3.IsRectangle(),  "Pas Triangle Rectangle");
		Assertions.assertTrue(t4.IsRectangle(),  "Pas Triangle Rectangle");
	}
    

}
